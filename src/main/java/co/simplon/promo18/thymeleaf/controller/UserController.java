package co.simplon.promo18.thymeleaf.controller;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo18.thymeleaf.entity.User;
import co.simplon.promo18.thymeleaf.repository.UserRepository;


@Controller
public class UserController {
    @Autowired
    private UserRepository repo;
    @Autowired
    private PasswordEncoder encoder;

    @GetMapping("/register")
    public String showRegister(Model model) {
        model.addAttribute("user", new User());
        return "register";
    }

    @PostMapping("/register")
    public String register(User user, Model model) {
        String hash = encoder.encode(user.getPassword());
        
        user.setPassword(hash);
        try {
            repo.save(user);

        } catch(DataIntegrityViolationException e) {
            
            model.addAttribute("user", user);
            model.addAttribute("error", "User already exists");
            return "register";
        }

        
        return "redirect:/login";
    }
    
}
