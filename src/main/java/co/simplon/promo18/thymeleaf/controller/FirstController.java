package co.simplon.promo18.thymeleaf.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;


@Controller
public class FirstController {
    
    @GetMapping("/")
    public String home(Model model) {
        List<String> list = new ArrayList<>(List.of("ga", "zo","bu", "meu"));

        String name = "John";

        model.addAttribute("name", name);
        model.addAttribute("list", list);
        
        return "first";
    }

    @GetMapping("/greeting/{name}")
    public String withParameter(@PathVariable String name, Model model) {
        model.addAttribute("name", name);
        return "first";
    }

    @GetMapping("/form")
    public String form() {

        return "form";
    }

    @PostMapping("/form")
    public String formProcess(@ModelAttribute("example") String example) {
        System.out.println(example);
        return "form";
    }

    
    
}
