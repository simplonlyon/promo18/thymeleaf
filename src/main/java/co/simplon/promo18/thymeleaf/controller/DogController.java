package co.simplon.promo18.thymeleaf.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo18.thymeleaf.entity.Dog;
import co.simplon.promo18.thymeleaf.repository.DogRepository;

@Controller
public class DogController {
    
    @Autowired
    private DogRepository repo;

    @GetMapping("/dog")
    public String showDogs(Model model) {

        model.addAttribute("dogs", repo.findAll());
        return "dog/list-dog";
    }


    @GetMapping("/dog/{id}")
    public String singleDog(@PathVariable int id, Model model) {

        Dog dog = repo.findById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

        model.addAttribute("dog", dog);
        return "dog/single-dog";
    }


    @GetMapping("/add-dog")
    public String showForm(Model model) {

        model.addAttribute("dog", new Dog());
        return "dog/add-dog";
    }

    @GetMapping("/edit-dog/{id}")
    public String editForm(@PathVariable int id,Model model) {
        Dog dog = repo.findById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        model.addAttribute("dog", dog);
        return "dog/add-dog";
    }

    @PostMapping("/add-dog")
    public String addDog(Dog dog) {
        repo.save(dog);
        return "redirect:/dog/"+dog.getId();
    }
}
