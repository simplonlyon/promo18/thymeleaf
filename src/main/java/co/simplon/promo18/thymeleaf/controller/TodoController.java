package co.simplon.promo18.thymeleaf.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class TodoController {
    private List<String> list = new ArrayList<>(List.of("valeur 1", "valeur 2"));
    
    @GetMapping("/todo")
    public String display(Model model) {
        model.addAttribute("list", list);
        return "todo";
    }

    @PostMapping("/todo")
    public String addTodo(@ModelAttribute("newTodo") String newTodo) {
        list.add(newTodo);
        return "redirect:/todo";
    }

    @PostMapping("/todo/delete/{toDel}")
    public String removeTodo(@ModelAttribute("toDel") int toDel) {
        list.remove(toDel);
        return "redirect:/todo";
    }
}

